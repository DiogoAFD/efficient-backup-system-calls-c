CC=gcc
CFLAGS=

all: sobucli sobusrv 

cliente: 
	$(CC) $(CFLAGS) sobucli.c -o sobucli

servidor: 
	$(CC) sobusrv.c -o sobusrv

clean:
	rm -f sobusrv sobucli
