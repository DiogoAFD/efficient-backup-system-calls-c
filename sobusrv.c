#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <stdlib.h>

char** separa_espaco(char* abc, int d){
	int size=1;
	int max_size=d+1;
	char** ret = (char**)malloc(sizeof(char*)*max_size);
	char* token = strtok(abc," ");

	while(token!=NULL){
		ret[size++] = strdup(token);
		if(size == max_size){
			max_size++;
			ret=(char**)realloc(ret,sizeof(char*)*max_size);
		}
		token=strtok(NULL," ");
	}
	ret[size]=NULL;

	return ret;
}




int main(int argc, char *argv[]){ 


	int k = mkfifo("../.Backup/pipeClienttoServer", 0666);
	int status;
	char buffer[1024];
	int t;
	char * frase;
	char* palavras[1000];
	char* path_data = "../.Backup/data/";
	char* path_metadata = "../.Backup/metadata/";

	while(1) {
		int cspipe = open("../.Backup/pipeClienttoServer",O_RDONLY,0666);

		while( (t=read(cspipe,buffer,1024) >0));
		
		//int criafilho=fork();
		//if(!criafilho){
		
			frase = strtok(buffer," ");
			int i;
			for (i = 0; frase != NULL; i++){
				palavras[i]=frase;
				frase = strtok(NULL," ");
			}
			i--;
			pid_t pidCliente=atoi(palavras[i]);

			int nrFicheiros=i-1;
			char c;
			int j;
			int erroExec=0;

			if(strcmp(palavras[0],"backup")==0){  

				for(j=1;j<=nrFicheiros && erroExec == 0;j++){

					erroExec=0;

					char *file2= (char*)malloc(100*sizeof(char));
					strcat(file2,palavras[j]);
					strcat(file2,".gz");

					
					int fd2 = open("saida.txt",O_RDWR | O_TRUNC | O_CREAT, 0666); 
					dup2(fd2,1); 
					close(fd2);


					int fderro = open("erro.txt",O_RDWR | O_TRUNC | O_CREAT, 0666); 
					dup2(fderro,2); 
					close(fderro);


					int p= fork();
					if(!p){ // FILHO
						erroExec= execlp("sha1sum","sha1sum",palavras[j],NULL);
						kill(pidCliente,SIGINT); //// nem preciso do if pois só chega aqui se o exec falhar
						
					}
					else{
						wait(&status);
						if(erroExec!=-1){
							
							int fd3 = open("saida.txt",O_RDONLY, 0666);
							int fderro2=open("erro.txt",O_RDONLY, 0666);
							char buff_aux1[1024];
							int m=0;
							char* ressha1sum = (char*)malloc(sizeof(char)*100);
							char* str = (char*)malloc(sizeof(char)*100);
							while(read(fd3,buff_aux1+m,1)>0){
								if(buff_aux1[m]=='\n'){
									char**args = separa_espaco(buff_aux1,3);
									strcat(ressha1sum," "); 
									strcat(ressha1sum,args[1]);
									strcat(str, " ");
									strcat(str,args[2]);
									m=0;
								}
								m++;
							}
							for(m=0;ressha1sum[m]!='\0';m++){ 
								ressha1sum[m]=ressha1sum[m+1];
							}

							for(m=0;str[m]!='\0';m++){ 
								str[m]=str[m+1];
							}
							if(strlen(ressha1sum)==0){
								kill(pidCliente,SIGINT);
							}
							else{
								p=fork();
								if(!p){
									erroExec=execlp("gzip","gzip","-k",palavras[j],NULL);
									kill(pidCliente,SIGINT);		
								}
								else{
									wait(&status);
									if(erroExec!=-1){
										char *file3= (char*)malloc(100*sizeof(char));
										strcat(file3,ressha1sum);
										strcat(file3,".gz");

										p=fork();
										if(!p){
											erroExec = execlp("mv","mv",file2,file3,NULL);
											kill(pidCliente,SIGINT);
											
										}
										else{
											wait(&status);
											if(erroExec!=-1){
												p=fork();
												if(!p){
													erroExec = execlp("mv","mv",file3,path_data,NULL); 
													kill(pidCliente,SIGINT);
													
												}
												else{
													wait(&status);
													if(erroExec!=-1){
														p=fork();
														if(!p){
															char *filedata= (char*)malloc(100*sizeof(char));
															char *filemetadata= (char*)malloc(100*sizeof(char));

															strcat(filedata,"../data/"); 
															strcat(filedata,file3);		
															strcat(filemetadata,path_metadata);	
															strcat(filemetadata,palavras[j]);		
															erroExec = execlp("ln","ln","-sf",filedata,filemetadata,NULL); 
															kill(pidCliente,SIGINT);		
														}
														else{
															wait(&status);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				int filhop=fork();
				if(!filhop){
					execlp("rm","rm","saida.txt",NULL);
					kill(pidCliente,SIGINT);
				}
				else{
					wait(&status);
					kill(pidCliente,SIGUSR1);
				}
			}

			else if(strcmp(palavras[0],"restore")==0){ 

				for(j=1;j<=nrFicheiros && erroExec ==0 ;j++){
					int fd_readlink = open("readlink.txt",O_RDWR | O_TRUNC | O_CREAT, 0666);
					dup2(fd_readlink,1); 
					close(fd_readlink);
					int p= fork();


					if(!p){ // FILHO
						char* file_readlink= (char*)malloc(100*sizeof(char));
						strcat(file_readlink,path_metadata);  	
						strcat(file_readlink,palavras[j]);		
						erroExec = execlp("readlink","readlink",file_readlink,NULL);
						kill(pidCliente,SIGINT);
					}
					else{
						wait(&status);
						if(erroExec!=-1){
							int fd_readlink2 = open("readlink.txt",O_RDONLY, 0666);
							char buff_aux2[1024];
							int m=0;
							char* path_readlink = (char*)malloc(sizeof(char)*100);
							
							while(read(fd_readlink2,buff_aux2+m,1)>0){
								
								if(buff_aux2[m]=='\n'){
									strcat(path_readlink,buff_aux2);
									m=0;
								}
								m++;
							}
							if(strlen(path_readlink)==0){
								kill(pidCliente,SIGINT);
							}
							else{
								int tamanho=strlen(path_readlink);
								int z;
								char* path_limpa = (char*)malloc(sizeof(char)*100);
								for(z=0; z<tamanho-6;z++){
									path_limpa[z]=path_readlink[z];
								}
								path_limpa[z]='\0';
								

								char* path_gunzip=(char*)malloc(sizeof(char)*100);;
								strcat(path_gunzip, "../.Backup/data/");
								strcat(path_gunzip, path_limpa);

								p=fork();
								if(!p){
									erroExec = execlp("gunzip","gunzip","-k","-f",path_gunzip,NULL);
									kill(pidCliente,SIGINT);
									
								}
								else{
									wait(&status);
									if(erroExec!=-1){
										p=fork();
										if(!p){
											erroExec = execlp("mv","mv",path_gunzip,palavras[j],NULL);
											kill(pidCliente,SIGINT);
										}
										else{
											wait(&status);					
										}
									}
								}
								int pp=fork();
								if(!pp){
									execlp("rm","rm","readlink.txt",NULL);
									kill(pidCliente,SIGINT);
								}
								else{
									wait(&status);
									kill(pidCliente,SIGUSR2);
								}
							}
						}
					}
				}

				
			}

			else{
				for(j=1;j<=nrFicheiros && erroExec ==0 ;j++){
					erroExec=0;
					int p= fork();
					if(!p){ 
						char* path_delete=(char*)malloc(sizeof(char)*100);;
						strcat(path_delete, "../.Backup/metadata/");
						strcat(path_delete, palavras[j]);
						erroExec = execlp("rm","rm",path_delete,NULL);
						kill(pidCliente,SIGINT);
					}
					else{
						wait(&status);
						kill(pidCliente,SIGTERM);
					}
				}
			}

			
			close(cspipe);
		//}	
	}
	
	
	return 0;
}
